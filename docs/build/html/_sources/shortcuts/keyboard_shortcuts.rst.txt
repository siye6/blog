.. _keyboard_shortcuts:
   
==========
键盘快捷键
==========

移动窗口到不同的显示器
======================

WIN
    - ALT+WIN+SHIFT+Arrow key

MAC
    - 系统设置，键盘快捷键设置，APP快捷键，然后根据需要设置对应的快捷
      键；比如：移到“内建视网膜显示器”

    .. _fig_customize_shortcuts:
    .. figure:: /www/customize_shortcuts.png
       :scale: 70%
