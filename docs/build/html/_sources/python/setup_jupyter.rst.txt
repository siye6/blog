.. _setup_jupyter:

======================
 启用Jupyter notebook
======================

为什么
======

作为一款开源的网络应用Jupyter notebook可以创建和分享包含可交互的程序，
文本，可视化信息等等，在很多领域都有使用包括：数据分析，数据清理，机器
学习，数据可视化，统计模型，等等。它支持超过40种流行的语言，比如R，
Python，Julia。可交互，可高度客制化，很方便的进行分享，实时编程并显示
结果。

怎么做
======

- 安装Jupyter

  1) pip

     - 可以使用pip安装:

     .. code-block:: bash

           pip install jupyter


  2) Anaconda or conda

     - 也可以直接通过Anaconda或者是conda来安装： Anaconda已经默认包括
       了Jupyter所以无需额外安装，如果使用conda, 那么可以通过conda命令
       来安装：

     .. code-block:: bash

           conda install jupyter

- 开启Jupyter服务: 直接在你想要启用Jupyter notebook的路径输入如下命令
  即可：

  .. code-block:: bash

        jupyter notebook

真是太简单了！

- 补充：默认开启Jupyter notebook会随机为这个notebook生成一个token。如
  果我们想改成自己的密码可以在控制台为Jupyter生成配置文件，然后在相应
  的配置文件当中查找token，将默认的值generate修改成自己的密码即可。

  .. code-block:: bash

     jupyter notebook config


