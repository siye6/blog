.. _pytest01:

===============================
 pytest cannot find the module
===============================

Issue
=====

My folder layout like below, which should be a quit common folder
layout(at least from my side), however, the pytest runner keep
reporting errors that cannot find the module.

.. line-block::

   .
   ├── LICENSE
   ├── src
   │   └── pkg
   │       ├── __init__.py
   │       └── jmsbw.py
   └── tests
       └── unit
           └── test_return_pct.py

Solution
========

It is because pytest don't know that path of the package, therefore,
we have to specify the package path in the config file
``pyproject.toml``

.. code-block:: toml

   [tool.pytest.ini_options]
   pythonpath = ["src"]                

.. line-block::

   .
   ├── LICENSE
   ├── pyproject.toml
   ├── setup.cfg
   ├── src
   │   └── pkg
   │       ├── __init__.py
   │       └── jmsbw.py
   └── tests
       └── unit
           └── test_return_pct.py
   
