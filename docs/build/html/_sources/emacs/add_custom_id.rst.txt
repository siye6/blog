=====================================
Use YAS create the CUSTOM\ :sub:`ID`\
=====================================

    :Author: Siye Liu

.. contents::

.. _custom_id:

1 Use YAS to create the CUSTOM\ :sub:`ID`\
------------------------------------------

.. _what:

1.1 What
~~~~~~~~

This article explain how to make internal link with custom\ :sub:`id`\ and
easily insert custom\ :sub:`id`\ with YAS.

.. _why:

1.2 Why
~~~~~~~

When we write the docs it is always useful to create the internal &
external link. For internal link, you can always insert the \`[[]]\` and
insert the text to looking for. Org by default will search the
headline, however this will require we enter the headline title(some
times it is too long), therefore using a \`custom\ :sub:`id`\\` is an good way to
address this issue.

.. _how:

1.3 How
~~~~~~~

Custom id is one of the properties, its format looks like

.. code:: elisp

    :PROPERTIES:
    :CUSTOM_ID: <your custom_id>
    :END:

Therefore, in order to write the custom\ :sub:`id`\ by myself, I could use YAS
create the shortcuts to write the same text and leave the cursor at
\`custom\ :sub:`id`\\` for me to enter the name of the ID.
