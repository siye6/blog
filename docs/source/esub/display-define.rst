#+begin\ :sub:`example` rst .. :sub:`display`-define:

#+end\ :sub:`example`

Display the define.xml
======================

What
----

Find the solution to open the define.xml file via ``EDGE`` / ``Chrome
browser``.

Why
---

By default use the IE browser to open the define.xml file. However,
after IE11 **decommission**, this option will be no longer available.

How
---

The reason why ``EDGE`` / ``Chrome`` not supporting open the define.xml
file is because there're securtiy issues if it enable the script
locally. To make it work we could add parameter
**-—allow-file-access-from-files** when run ``EDGE`` / ``Chrome``.

From windows terminal
~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   start msedge --allow-file-access-from-files

From shortcuts
~~~~~~~~~~~~~~

#. Right hit the shortcut link of EDGE / Chrome and select the
   properties. |image0|
#. In the ``target`` box, add **-–allow-file-access-from-files** |image1|

.. |image0| image:: screenshot/properties.png
.. |image1| image:: screenshot/parameter.png
