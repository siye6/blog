.. pvalue01

.. _pvalue01:

===============
 p-value （一）
===============


p-value的定义
=============

p-value is the probability that random chance generated the data, or
something else that is equal or rarer.

我自己的理解：p-value就是三个事件的概率（可能性）之和。

- 事件一：当前我们感兴趣的事件
- 事件二：和我们感兴趣的事件机会均等的事件
- 事件三：比我们感兴趣的事件机会更小的事件

为什么这么奇葩
==============

试着这么理解：假如老婆生日，我送她一个宝石，并且告诉她

1) 这是世界上最美丽的宝石。
2) 这是世界上最美丽的宝石之一，还有一些宝石和她一样珍贵。
3) 这是世界上美丽的宝石，还有一些宝石和她一样珍贵，当然最珍贵的宝石我
   们都没有机会见过。

这三个句式会让人对这个宝石的期待越来越低，最后觉得根本没啥。换言之，这
个事件并没有那么特殊，没有啥显著的意义。那么如果我们在检验当中算出来
p-value小于一个不太可能发生的概率（比如0.05）那么就能说明我们之前的设
定不太可能正确。

基本的计算
==========

如果某个地区的身高是一个正态分布的曲线，1.4米到1.6米之间发生的概率是
95%，那么小于1.4米的p-value是多少？

小于1.4米的概率和大于1.6米的概率都是相等的，它们的和是1-95%=5%。所以小于
1.4米的p-value = 小于1.4米的概率5%/2 + 大于1.6米的概率5%/2 + 比小于1.4米
更不常见的身高（0）即0.05。

如果在曲线的中间取一小段：例如1.554米到1.557米。这个区间的概率是4%，小
于1.554米的概率是48%，大于1.557米的概率是48%，那么1.554米到1.557米之间
的p-value是多少？

1\.554米到1.557米之间的p-value等于1.554米到1.557米之间的概率4%加上与之
相同概率事件的概率（0）加上比它更不常见的事件的概率（因为1.554米到
1.557米的区间是在正态曲线的最中间，其他的事件都比这个事件更不常见48%
乘以2即100%。
