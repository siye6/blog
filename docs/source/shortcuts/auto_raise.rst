.. _auto_raise:

Auto Raise The Window When Hover It
===================================

What
----

Find a solution OR `APP <https://github.com/sbmpost/AutoRaise>`__ to
help auto raise / focus the window when the mouse hover on it.

Why
---

There are two ways to switch between different APPs(assum they are
located in different area of the monitor by window management tools),
keyboard ``COMMAND-TAB`` OR move mouse / trackpad to the window and hit
it. Both are annoying. Keyboard shortcuts is too slow especially when
you have more then 4 APPs open, it takes while and stop you
thinking(because it force you to identify the correct APP icon), moving
mouse / trackpad sound good but you have to hit the left key OR
trackpad, there are two actions now. Why don't just hover the mouse to
the right window and let the APP help you to automatically raise it?

How
---

`AutoRaise <https://github.com/sbmpost/AutoRaise>`__ is the right tool
to resolve this problem.

Reference
---------

`AutoRaise README <https://github.com/sbmpost/AutoRaise>`__
