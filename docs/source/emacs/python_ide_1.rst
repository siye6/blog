.. _python_ide01:

===============================
 配置EMACS作为python IDE（一）
===============================

更改默认EMACS的python插件
=========================

elpy包比emacs自带的python模式支持的更多，所以最好用它来替换默认的
python模式。

   ``M-x package-list-packages``

查找elpy包并安装。

在init.el中启用elpy包。

   ``(require 'elpy)``

增加pep8规则
============

通过conda安装autopep8插件，它可以帮助来自动完成pep8规范检查并纠正。

.. code-block:: bash

   conda install autopep8

在init.el中启用py-autopep8。

   ``(require 'py-autopep8)``
   ``(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)``

python-shell-interpreter的更换
==============================

EMACS默认会用系统自带的python而不是我们通过conda安装的python版本，这样
很多通过conda安装的包都无法使用，需要修改配置来更换python的路径。可以
通过customize-options，更改python-shell-interpreter的值来替换默认路径。

Jupyter notebook的支持
======================

Jupyter notebook用来学习和使用python特别方便。这里我们也可以直接在
emacs里面打开Jupyter notebook。

- 安装ein插件并启用

   ``(require 'ein)``

- 在控制台或者eshell启用Jupyter notebook。参见 :ref:`setup_jupyter`.

- 通过ein打开notebooklist

   ``M-x ein:notebooklist-open``

- 输入密码或者token，进入notebook。选择以有的notebook或者新建notebook
  开始使用。

      
