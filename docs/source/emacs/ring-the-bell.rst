=============
Ring the bell
=============

    :Author: Siye Liu

.. contents::

.. _ring-the-bell:

1 What
------

Find a way to stop emacs beep if there is any operation error.

2 Why
-----

Every time when you type something which is not allowed or wrong,
emacs will beep to warning you. However, ``C-g`` is also included which
is very annoying. So it is better to find a way to stop it.

3 How
-----

Hit ``C-h a`` to search any command / function related to this beep,
nothing found. Try ``M-x apropos`` and search ``sound`` ``ring`` or ``bell``
finnaly found this function called ``ring bell function``. The manual
says the default value is nil, and if it is not nil then ring the
bell. So if we can make it always nil then it should be no more
ring. There are two functions related to this, one is ``always`` it
always return to ``t``, another one is ``ignore``, it will always return
``nil``. So we could use:

.. code:: common-lisp

    (setq ring-bell-function 'ignore)

in our config file, the world will be quiet next time.
