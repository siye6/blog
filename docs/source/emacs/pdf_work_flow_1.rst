.. _pdf_work_flow_1:
.. _pdf-view-annotation-work-flow-1:

PDF View / Annotation Work Flow
===============================

What
----

Find tools to streamline the workflow in PDF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `PDF-tools <https://github.com/politza/pdf-tools>`__
-  `org-pdftools <https://github.com/fuxialexander/org-pdftools>`__

Why
---

There are some challenges if you use Adobo PDF Read / PDF Xchange when:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Present the annotation of PDF during the meeting is painful since the
   annotation size is very small, and it required a lot of mouse click
   to show the annotations.
-  May need to quickly jump from one annotation to another OR another
   page OR do a quick search, sometimes the search function is not that
   good in the adobe PDF read / PDF changeX.
-  There maybe some actions / notes during the meeting. If we could take
   the notes within the PDF and also export a clear doc that would be
   very helpful for team to track.
-  Edit experience (for annotation) is really very bad in the current
   PDF software.

How
---

Installation
~~~~~~~~~~~~

#. MacOS

   #. Install poppler & automake

      .. code:: bash

         brew install poppler automake

   #. Set the env path for pkg-config

      .. code:: bash

         export PKG_CONFIG_PATH="/usr/local/Cellar/pkg-config:/usr/local/lib/pkgconfig"

   #. Install ``pdf-tools`` from MELPA

   #. Activate ``pdf-tools``

      .. code:: elisp

         (pdf-tools-install)

   #. Install ``org-pdftools`` from MELPA

   #. Activate ``org-pdftools``

      .. code:: elisp

         (require 'org-pdftools)
         (add-hook 'pdf-view-mode-hook 'org-pdftools-setup-link)
         (add-hook 'org-mode-hook 'org-pdftools-setup-link)

Use Case
~~~~~~~~

#. Present PDF

   Start ``emacs`` open the PDF from ``dired``. ``+`` ``-`` to zoom in /
   out; ``P`` fit to page, ``W`` fit width, ``H`` fit Height.

#. Jump to annotation from list

   ``C-c C-a l`` List annotation, hit ``SPACE`` to jump to the
   annotation from list.

#. Edit annotation

   ``C-c C-a l`` List annotation, hit ``RET`` to edit the annotation;
   via Mouse selection and left-click add annotation.

#. Store link in org

   ``C-c l`` OR ``M-x org-store-link`` to store the link in current
   location and swtich to org buffer ``C-c C-l`` insert link. Via mouse
   hit the link to jump back the location of the PDF file.

Reference
---------

`PDF-tools <https://github.com/politza/pdf-tools>`__
`org-pdftools <https://github.com/fuxialexander/org-pdftools>`__
