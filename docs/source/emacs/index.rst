.. This is the toc of emacs

================
Emacs Everyday
================

.. toctree::
   :maxdepth: 2

   recursive_find_1
   python_ide_1
   pdf_work_flow_1
   open_init
   ring-the-bell
   add_custom_id
