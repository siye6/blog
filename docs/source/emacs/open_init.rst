.. _open_init:

.. _key-binding-to-open-emacs-configuration-file-1:

Key Binding To Open Emacs Configuration File
============================================

What
----

Create a function open the emacs configuration file and binding a key to
it.

Why
---

It is very frequently to open the emacs configuration file and add some
more function since I am using a basic emacs(compare to space-emacs /
doom emacs). Therefore, it is essential to have a keybaord shotcuts
binding to this task, otherwise, you have to always type ``C-x f``
``~/.emacs.d/init.el`` ``RET``, which is a little bit annoy.

How
---

Add below code block in the emacs configuration file.

.. code:: elisp


   ;; Define a function to open init file
   (defun edit-init-file()
     "edit init file helps to open the ~/.emacs/init.el file"
     (interactive)
     (find-file "~/.emacs.d/init.el"))

   ;; Binding a key to the function edit-init-file
   (global-set-key (kbd "ESC M-i") 'edit-init-file)
