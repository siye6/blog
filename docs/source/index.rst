.. Blog documentation master file, created by
   sphinx-quickstart on Sat Aug 28 22:38:23 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Greeting
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   *
   badminton/index
   sas/index
   coding/index
   shortcuts/index
   python/index
   emacs/index
   statistic/index
   mix/index
   esub/index
   presentation/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
